package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpStockOutLog;

/**
 * 出库日志Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpStockOutLogService 
{
    /**
     * 查询出库日志
     * 
     * @param id 出库日志ID
     * @return 出库日志
     */
    public ErpStockOutLog selectErpStockOutLogById(String id);

    /**
     * 查询出库日志列表
     * 
     * @param erpStockOutLog 出库日志
     * @return 出库日志集合
     */
    public List<ErpStockOutLog> selectErpStockOutLogList(ErpStockOutLog erpStockOutLog);

    /**
     * 新增出库日志
     * 
     * @param erpStockOutLog 出库日志
     * @return 结果
     */
    public int insertErpStockOutLog(ErpStockOutLog erpStockOutLog);

    /**
     * 修改出库日志
     * 
     * @param erpStockOutLog 出库日志
     * @return 结果
     */
    public int updateErpStockOutLog(ErpStockOutLog erpStockOutLog);

    /**
     * 批量删除出库日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpStockOutLogByIds(String ids);

    /**
     * 删除出库日志信息
     * 
     * @param id 出库日志ID
     * @return 结果
     */
    public int deleteErpStockOutLogById(String id);
}
