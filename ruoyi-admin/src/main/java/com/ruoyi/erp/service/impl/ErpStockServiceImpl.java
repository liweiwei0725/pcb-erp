package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpStockMapper;
import com.ruoyi.erp.domain.ErpStock;
import com.ruoyi.erp.service.IErpStockService;
import com.ruoyi.common.core.text.Convert;

/**
 * 产品库存Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpStockServiceImpl implements IErpStockService 
{
    @Autowired
    private ErpStockMapper erpStockMapper;

    /**
     * 查询产品库存
     * 
     * @param id 产品库存ID
     * @return 产品库存
     */
    @Override
    public ErpStock selectErpStockById(String id)
    {
        return erpStockMapper.selectErpStockById(id);
    }

    /**
     * 查询产品库存列表
     * 
     * @param erpStock 产品库存
     * @return 产品库存
     */
    @Override
    public List<ErpStock> selectErpStockList(ErpStock erpStock)
    {
        return erpStockMapper.selectErpStockList(erpStock);
    }

    /**
     * 新增产品库存
     * 
     * @param erpStock 产品库存
     * @return 结果
     */
    @Override
    public int insertErpStock(ErpStock erpStock)
    {
        erpStock.setId(IdUtils.fastSimpleUUID());
        erpStock.setCreateTime(DateUtils.getNowDate());
        return erpStockMapper.insertErpStock(erpStock);
    }

    /**
     * 修改产品库存
     * 
     * @param erpStock 产品库存
     * @return 结果
     */
    @Override
    public int updateErpStock(ErpStock erpStock)
    {
        erpStock.setUpdateTime(DateUtils.getNowDate());
        return erpStockMapper.updateErpStock(erpStock);
    }

    /**
     * 删除产品库存对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpStockByIds(String ids)
    {
        return erpStockMapper.deleteErpStockByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品库存信息
     * 
     * @param id 产品库存ID
     * @return 结果
     */
    @Override
    public int deleteErpStockById(String id)
    {
        return erpStockMapper.deleteErpStockById(id);
    }
}
