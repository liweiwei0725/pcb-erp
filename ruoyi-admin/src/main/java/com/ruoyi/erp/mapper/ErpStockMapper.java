package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpStock;

/**
 * 产品库存Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpStockMapper 
{
    /**
     * 查询产品库存
     * 
     * @param id 产品库存ID
     * @return 产品库存
     */
    public ErpStock selectErpStockById(String id);

    /**
     * 查询产品库存列表
     * 
     * @param erpStock 产品库存
     * @return 产品库存集合
     */
    public List<ErpStock> selectErpStockList(ErpStock erpStock);

    /**
     * 新增产品库存
     * 
     * @param erpStock 产品库存
     * @return 结果
     */
    public int insertErpStock(ErpStock erpStock);

    /**
     * 修改产品库存
     * 
     * @param erpStock 产品库存
     * @return 结果
     */
    public int updateErpStock(ErpStock erpStock);

    /**
     * 删除产品库存
     * 
     * @param id 产品库存ID
     * @return 结果
     */
    public int deleteErpStockById(String id);

    /**
     * 批量删除产品库存
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpStockByIds(String[] ids);
}
